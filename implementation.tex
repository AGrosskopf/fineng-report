\section{Implementation}\label{sec:implement}
In this section we outline the main components of our implementation. A greater level of detail is provided for components of significant importance or of sufficient complexity. We also relate what experience and lessons we have learnt from the implementation phase.

\subsection{Class Overview}\label{subsec:imp-class}
In this section we list most of the classes involved in our implementation. We explain their purpose and give some indication of the mechanism or mathematics which underlies them. When a class has non-trivial behavior we include a description of some its methods. Library classes and those from the C++ course are generally omitted from this list unless there is a compelling reason to include them.

\begin{table}[h!]
	\centering
		\begin{tabular} { |p{6cm}|p{10cm}| } 
 		\hline
  		Name & Purpose \\ 
 		\hline
 		\verb|ComplexMatrix| & Matrices dealing with complex numbers (modified from \verb|matrix|).\\
 		\hline
 		\verb|BaseInterp| & General interpolation procedures (Credit: Numerical Recipes).\\
 		\hline
 		\verb|FunctionBase| & A type-generic functor $f(x)$.\\
 		\hline
 		\verb|DifferentiableFunctionBase| & 
		A composite functor for $f(x)$ and $f'(x)$.\\
 		\hline
 		\verb|LinearComposition| & Represents a linear composition of \verb|DifferentiableFunctionBase|s $f(x) =  ag(x) + bh(x)$.\\
 		\hline
 		\verb|ModBesselIv| & Computes Modified Bessel Function of the first kind.\\
 		\hline
 		\verb|NewtonMethod| & Find functions roots by using Newton Method.\\
 		\hline
 		\verb|SplineInterpolation| & Interpolate option price along log-variance grid.\\
 		\hline
 		\verb|ContinuationFunction| & Represents the value of the options if the holder chooses not to exercise.\\
 		\hline
 		\verb|FangOosterlee| & Controller for the algorithm.\\
 		\hline
 		\verb|LogVarChF| & Log-variance characteristic functor.\\
 		\hline
 		\verb|LogVarConditionalChF| & Log-variance conditional characteristic functor.\\
 		\hline
 		\verb|LogVarPDF| & Probability density functor for the log-variance process.\\
 		\hline
 		\verb|MJ| & Determines matrices listed in \cite[p. 36]{Fang2009}\\
 		\hline
 		\verb|PayoffCOSCoeff| & Fourier-transformed payoff function\\
 		\hline
 		\verb|OptionBermudanBase| & Represents common functionality to all Bermudan options\\
 		\hline
 		\verb|OptionBermudanCall| & Implements payoff functions in the original and log-variance domain.\\
 		\hline
 		\verb|OptionBermudanPut| & Implements payoff functions in the original and log-variance domain.\\
 		\hline
 		\verb|ProcessHeston| & Calculates quantities relevant to Hestons process.\\
 		\hline
		\end{tabular}
	\caption{An overview of the classes implemented at part of this project}
	\label{tab:class-overview}
\end{table}

{\large\textbf{ComplexMatrix}}

This is a modified version of the \verb|matrix| class. It was modified to deal with complex numbers. A type-generic version of the matrix class was considered, but limitations surrounding casting to and from the complex domain under such a setup were too restrictive.
 
{\large\textbf{BaseInterp}}

BaseInterp class is taken from Numerical Recipes textbook. It is an superclass of SplineInterpolation class. The class was not modified sematically, though some syntactic modifications were made to ensure compatibility.
  
{\large\textbf{FunctionBase}} 
Represents a function of one variable $f(x)$ from a domain of type \verb|D| to a range of type \verb|R|. Typically we set \verb|D| $ = $ \verb|R| $ \in \{$ \verb|double|, \verb|std::complex<double>| $\}$.
  
{\large\textbf{DifferentiableFunctionBase}}
Acts as a functor for $f(x)$ via inherited characteristics from \verb|FunctionBase| and for $f'(x)$ natively.
  
{\large\textbf{LinearComposition}}
Represents a linear combination $af(x) + bg(y)$, where $f(x)$ and $g(y)$ are \verb|DifferentiableFunctionBase| with domain \verb|D| and range \verb|R|, and $a$ and $b$ are of type \verb|R|.
  
{\large\textbf{ModBesselIv}}

Computes the Modified Bessel Function of First Kind $I_{\nu}(z)$ of real order $\nu$ and complex argument $z$. For $z \in \mathbb{R}$ the Numerical Recipes implementation in \verb|rv_library| is used. Otherwise we use the Taylors series expansion as follows.
 
\begin{equation}
I_{\nu}(z) = \sum_{m=0}^{\infty}\frac{(-1)^{m}}{m! \Gamma (m + \nu  +1)}\left ( \frac{x}{2}\right )^{2m +\nu }  
\end{equation}

\begin{remark}[Convergence]
It is known that the above Taylor series is slow to converge. Our code computes only the first 10 terms. with further terms posing problems for \verb|double| arithmetic. Our experiments show this gives an accurate approximation to 5 decimal points.
\end{remark}

In our context it is known that $\nu \geq -1$. To compute the negative order $v$ we make use of the following recurrence relation.

\begin{equation}
I_{\nu}(z) = (2(\nu+1)/x) I_{\nu+1}(z) + I_{\nu+2}(z)
\end{equation}

\begin{remark}[Intricacies of the modified Bessel function]
Fang and Oosterlee recognize the difficulty in computing $I_{\nu}(z)$ when the Feller condition is not satisfied, I.E. $\nu \in (-1, 0)$. They cite \cite{Amos1985} who has developed a hybrid method which dynamically selects from a variety of underlying methods such as the above Taylor series, or the exponentially scaled Bessel function, to ensure accurate and stable computation across the domain $(\nu, z) \in \mathbb{R}_{\geq} \times \mathbb{C}$.
\end{remark}
  
{\large\textbf{SplineInterpolation}}

SplineInterpolation is taken from Numerical Recipes textbook. It implements cubic spline interpolation with optional parameters for the gradient at the end-points $f'(x_0)$ and $f(x_J)$, where the default is the natural spline $f'(x_0) = f'(x_J) = 0$.
  
{\large\textbf{FangOosterlee}}

\verb|FangOosterlee| houses the main thrust of our code. The constructor accepts a PseudoFactory object which will pass all the relevant variables. It has the following notable member functions.

\verb|DetermineTruncationRanges()| computes the truncation ranges $[a_v, b_v]$ in the log-variance dimension using Newtons method and $[a, b]$ in the log-stock dimension.

\verb|SetupQuadrature()| computes the quadrature nodes and weights. An equidistant quadrature rule is used with weights
\begin{equation}
w_{i} = 
  \begin{cases} 
      \hfill 1    \hfill & i = 1, 2, ..., J - 2 \\
      \hfill \frac{1}{2} \hfill & i = 0, J - 1 \\
  \end{cases}
\end{equation}

We originally implemented a Gaussian quadrature with $J = 8$ nodes. However, we found that having a small number of nodes was detrimental to the accuracy of the valuation. The above quadrature is easily implemented for arbitrary $J$.

\verb|ComputeVtM()| function calculates the the $N$ by $N$ matrix $V(t_M)$ at expiry time $t_M$. The element $v_{i,j}$ of $V(t_m)$ is the $i^{\text{th}}$ Fourier cosine coefficient of the Bermudan's value function at time $t_m$ assuming $\sigma = \zeta_j$. Thus, since at time expiry time $t_M$ the option's value function is equal to its payoff function, the matrix $V(t_M)$ is composed of the cosine coefficients of the payoff function. Therefore the function relies on the \verb|PayoffCOSCoeff| class in order to compute each element.

\verb|ComputePhi()| function prepares the matrix $\tilde{\phi(\zeta_j)}$ for all $j$ = 0, 1, … , $J – 1$. All elements inside come from the multiplication of the log-variance function $p_{\ln(\nu)}(\sigma_t|\sigma_s)$ and the conditional characteristic function $\phi(\omega; x_s, \sigma_t, \sigma_s)$. Therefore this function will call the \verb|LogVarConditionalChF| class and the \verb|LogVarPDF| class to construct the conditional characteristic function and the log-variance process respectively. The \verb|LogVarConditionalChF| class also calls the \verb|LogVarChF| class to calculate the characteristic function of the time-integrated variance.

\verb|Run()|
The initial phase of the algorithm follows these steps. 
\begin{enumerate}
	\item Firstly the \verb|ComputePhi| calls \verb|DetermineTruncationRanges()| to compute $[a, b]$ and $[a_v, b_v]$.
	\item Next \verb|SetupQuadrature()| is called.
	\item Following from that the matrix $V(t_M)$ of the cosine coefficients of the payoff function is determined via \verb|ComputeVtM()|. 
	\item For each $ j = 0, 1, ..., J - 1$, \verb|ComputePhi()| is called to to compute $\tilde{\phi(\zeta_j)}$.
\end{enumerate}

The next phase is contained within a loop over two variables, $m = M - 1, M - 2, ..., 1$ and $p = 0, 1, ... J - 1$. Inside this loop 
\begin{enumerate}
	\item We determine the early exercise price $x^*(\zeta_p, t_m)$ by calling \verb|DetermineEarlyExercise()|.
	\item Using this price we can determine the values of $l, u \in \{a, b, x^*(\zeta_p, t_m)\}$ which are set according to whether the option is a put or call.
	\item This enables us to calculate the $N$ by $N$ matrices $M_s(l, u)$ and $M_c(l, u)$ via \verb|GetMsMatrix()| and \verb|GetMcMatrix()| respectively.
	\item We also compute the $N$ by $J$ matrix $\hat{B}$ 
	\item We calculate $C(t_{m-1})$ using the above-mentioned quantities.
	\item Finally, we can recover $V(t_{m-1})$ from either $C(t_{m-1})$ or $G(t_{m-1})$ depending on how $\sigma_0$ relates to $x^*(\zeta_p, t_m)$.
\end{enumerate}
We repeat this process until at time $t_1$ when we know we can apply the continuation function recover $v(x_0, \zeta_p, t_0)$ for each $p = 0, 1, ..., J - 1$. Now we use \verb|SplineInterpolation| to find a value for $v(x_0, \sigma_0, t_0)$. This is our option value.

\verb|OptionBermudanBase|
This class is a sub-class of \verb|OptionBase|. This class is abstract but not pure. It is abstract because we found a lot of functionality common to both \verb|OptionBermudanPut| and \verb|OptionBermudanCall|. However it is not pure because the payoff functions remain undetermined.