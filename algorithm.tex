The algorithm under discussion in \cite{FangOosterleeOriginalPaper} aims to price Bermudan options under Heston's stochastic volatility model. In this section we will first introduce Bermudan options and Heston's model. Then we will turn our attention to the theoretical foundation of the algorithm. Finally we will discuss the exact mechanism of the algorithm.

A Bermudan option is one which allows the holder to exercise early at one of a specified finite set of times or periods before expiry. In much the same way as the real-world Bermuda is located between America and Europe, a Bermudan option is conceptually between an American option and a European option.

A stochastic volatility model (SVM) is one which assumes that the underlying price and it is volatility both follow stochastic processes. \cite{Heston1993} 's SVM is one of the most widely known. At time $t$ we denote by $x_t$ the log-stock price and the associated variance is denoted by $\nu_t$. We model these dynamics using the following stochastic differential equations

\begin{equation}\label{eq:d-x}
dx_t = \bigg(\mu - \frac{1}{2}\nu_t \bigg)dt + \rho\sqrt{\nu_t}dW_{1,t} + \sqrt{1 - \rho^2}\sqrt{\nu_t}dW_{2,t}
\end{equation}

\begin{equation}\label{eq:d-nu}
d\nu_t = \lambda (\bar{\nu} - \nu_t) + \eta \sqrt{\nu_t} dW_{1,t}.
\end{equation}

Here $\mu$ is the drift rate of the underlying, $\lambda$ is the speed of mean reversion, $\bar{\nu}$ is the expected volatility rate and $\eta$ is the variance of the volatility process. The correlation between these two processes is given by $\rho$. In addition $W_{1,t}$ and $W_{2,t}$ are independent Brownian motions.

We observe that the Black-Scholes model is a special case Heston's SVM which arises when $\eta=0$.

When using the COS method, there is an unknown probability density function which is not known analytically. We resolve the problem by combining the COS method with Fourier based cosine expansions to the system.
 
In the no-arbitrage world, the price of the option at any exercise date is equal to the maximum between the continuation value and the payoff at that exercise point. Once the early-exercise points have been determined, then the Bermudan option price can be calculated by the following steps.

We are able to price an option by using risk-neutral valuation formula given by

\begin{equation}\label{eq:risk-free-valuation}
v(x_{s}, \sigma_{s}, s) = e^{-r(t-s)} \mathds{E}^{\mathds{Q}}_s [v(x_{t}, \sigma_{t}, t)]
\end{equation}

Where $s$ is a current time, $t$ is an expiry date with $0 < s < t$, $r$ is the risk-free rate and $\mathds{E}^{\mathds{Q}}_s$ denotes expectation under the risk-neutral measurement $\mathds{Q}$ given the information available at time $s$. 

\clearpage
As mentioned, a Bermudan option consists of several early-exercise dates. Here we illustrate $M$ early-exercise dates and define each time step with $t_m < t_{m+1}$ and $t_M = T$. At each exercise date $t_m < T$ we have the option of exercising or continuing to hold the option. The Bermudan option pricing formula can be read as 

\begin{equation}\label{eq:bermudan-value}
\begin{split} 
v(x_{t_m}, \sigma_{t_m}, t_m) =\begin{cases}
g( x_{t_m}, t_m) & \text{ for } m= M\\ 
\text{max}[ c(x_{t_m}, \sigma_{t_m}, t_m ), g( x_{t_m}, t_m)] & \text{ for } m= 1,2, ..., M-1 \\ 
c(x_{t_m}, \sigma_{t_m}, t_m ) & \text{ for } m= 0 
\end{cases}
\end{split} 
\end{equation}	
where $g(x_{t_m})$ is the payoff function and $c(x_{t_m}, \sigma_{t_m}, t_m )$ is the continuation value at time $t_m$.

The notation $x_{t_m}$ and $\sigma_{t_m}$ can be simplified as $x_m$ and $\sigma_{m}$, respectively. Hence, the continuation value at time $t_m$ can be defined as

\begin{equation}\label{eq:continuation-fn}
c(x_{m}, \sigma_{m}, t_m ) = e^{-r\Delta t} \mathds{E}^{\mathds{Q}}_{t_m} [v(x_{m+1}, \sigma_{m+1}, t_{m+1})]
\end{equation}
with $\Delta t = t_{m+1} - t_m$

In equation \eqref{eq:continuation-fn} the expectation term can be transformed into the joint probability density of the log-stock at a future time, given the log-variance at the current time. The continuation value can be written as follows.

\begin{equation}\label{eq:continuation-fn-sub}
\begin{split}
c(x_{m}, \sigma_{m}, t_m ) & = e^{-r\Delta t}
\cdot \int_{\mathds{R}} \bigg[ \int_{\mathds{R}} v(x_{m+1}, \sigma_{m+1}, t_{m+1} ) p_{x\mid ln(v)}(x_{m+1}\mid \sigma _{m+1}, x_m, \sigma _{m})dx_{m+1} \bigg] \\ 
& \cdot p_{ln(v)}(\sigma _{m+1}\mid \sigma _{m}) d\sigma_{m+1}
\end{split}
\end{equation}

In the equation above, the term $p_{x \mid ln(v)}$, which is the probability density of the log-stock at a future time, given the log-variance at the current time, is not known analytically. In the COS method we can approximate the unknown probability density function from its Fourier cosine expansion.

The idea behind the COS method is to approximate the probability density function. In this paper, the Fourier cosine series expansion has a connection with characteristic function (ChF).

When evaluating the integral in equation \eqref{eq:continuation-fn-sub}
We consider a truncated integration range, [a, b] which is defined according to Fang and Oosterlee (2008)

\begin{equation}\label{eq:ab-range}
[a, b] = \bigg[\xi_{1} - 12\sqrt{\left | \xi _{2}\right |}, \xi_{1} + 12\sqrt{\left | \xi _{2}\right |}\bigg]
\end{equation}
where $ \xi_n$ is the $n$ th cumulative term of the log-stock process. We can approximate the probability density $p_{x \mid ln(v)}$ by Fourier cosine series expansion. We know from Fourier theory that the series can be approximated accurately by truncating all but the first N terms.

\clearpage
Hence the unknown probability density function $p_{x \mid ln(v)}$ can be expressed as 

\begin{equation}\label{eq:prob-x-given-lnv}
\begin{split}
p_{x\mid ln(v)}(x_{m+1}\mid \sigma _{m+1}, x_m, \sigma _{m}) &\approx
\displaystyle\sum_{n=0}^{N-1}{\vphantom{\sum}}' \frac{2}{b-a}\text{Re} \left \{ \varphi \bigg(\frac{n\pi }{b-a}; 0, \sigma _{m+1}, \sigma _{m}\bigg) e^{in \pi \frac{x_{m}-a}{b-a}} \right \} \\
& \cdot \cos\bigg(n\pi \frac{x_{m+1}-a}{b-a}\bigg)  
\end{split}
\end{equation}
$\Sigma ^{'}$ specifies the first element in the summation must be multiplied by one half. 

Here $ \varphi (\omega ; 0,\sigma_{m+1}, \sigma_{m})$ is characteristic function of $p_{x \mid ln(v)}$ and is given by 

\begin{equation}\label{eq:varphi}
\begin{split}
\varphi (\omega ; x_{s}, \sigma _{t}, \sigma _{s}) &= \text{exp}\bigg(i\omega \Big[ x_{s} +\mu(t-s)+\frac{\rho }{\eta }\big(e^{\sigma_{t}}-e^{\sigma_{s}}-\lambda \bar{v}(t-s)\big)\Big]\bigg)\\
& \cdot \Phi \bigg(\omega \Big(\frac{\lambda \rho }{\eta }-\frac{1}{2}\Big) + \frac{1}{2}i\omega ^{2}(1-\rho ^{2}); e^{\sigma _{t}},e^{\sigma _{s}} \bigg)
\end{split}
\end{equation}
$\Phi(u,\nu_t,\nu_s)$ is the characteristic function of the time integrated variance given in \cite{FangOosterleeOriginalPaper}.

Next step, we substitute $p_{x  \mid ln(v)}$, \eqref{eq:prob-x-given-lnv}, into \eqref{eq:continuation-fn-sub} to obtain the continuation value as follows.

\begin{equation}\label{eq:continuation_COS}
\begin{split}
c(x_{m}, \sigma _{m}, t_{m})= e^{-r\Delta t}\displaystyle\sum_{j=0}^{J-1} \omega _{j}\displaystyle\sum_{n=0}^{N-1}{\vphantom{\sum}}' V_{n,j}(t_{m+1}) \text{Re}\left \{ \tilde{\varphi }\Big(\frac{n\pi }{b-a}, \varsigma _{j}, \sigma _{m}\Big) e^{in\pi \frac{x_{m}-a}{b-a}} \right \}
\end{split}
\end{equation}
Where subscript $J$ comes from the outer integral in \eqref{eq:continuation-fn-sub} using a quadrature with $J$ nodes $\varsigma_1, \varsigma_2, ..., \varsigma_{J-1}$ and $N$ comes from the inner integral by truncating the infinite Fourier series to $N$ terms. 
The term $V_{n,j}(t_{m+1})$ represents the $n^{\text{th}}$ Fourier coefficient of the options value function at time $t_{m+1}$ at the quadrature node $\varsigma_j$. It is given by

\begin{equation}\label{eq:V_nj}
V_{n,j}(t_{m+1}) = \frac{2}{b-a}\int_{a}^{b}v(x_{m+1}, \varsigma _{j}, t_{m+1})\cos\Big(n\pi\frac{x_{m+1}-a}{b-a}\Big)dx_{m+1}.
\end{equation}
	
With Bermudan options as with American options there an is optimal exercise price which divides the log-stock dimension into exercise and continuation regions. We say that $x^*(\sigma_m,t_m)$ is an optimal exercise price at time $t_m$ ($m < M$) if the following equation is satisfied.

\begin{equation}\label{eq:c}
c(x_m, \varsigma_j, t_m) - g(x_m) = 0
\end{equation}
	
Since all the relevant quantities are available, we can search for the optimal exercise price using any root finding method such as Newton's method. Once we know the optimal exercise price we can compute the value of a Bermudan call at each early-exercise point $t_m$ ($m < M$) using

\begin{equation}\label{eq:early-exercise-call-val}
\begin{split}
v(x_{m}, \sigma_{m}, t_m) = \begin{cases}
  g( x_{m}, t_m) & \text{ for } x \in [a,x^*(\sigma_m,t_m)] \\
  c_3(x_{m}, \sigma_{m}, t_m ) & \text{ for } x \in [x^*(\sigma_m,t_m), b]
\end{cases}
\end{split}
\end{equation}

\clearpage
and similarly for a Bermudan put,

\begin{equation}\label{eq:early-exercise-put-val}
\begin{split}
v(x_{m}, \sigma_{m}, t_m) = \begin{cases}
  g( x_{m}, t_m) & \text{ for } x \in [a, x^*(\sigma_m,t_m)]  \\
  c_3(x_{m}, \sigma_{m}, t_m ) & \text{ for } x \in [x^*(\sigma_m,t_m), b].
\end{cases}
\end{split}
\end{equation}

At this point it is actually possible to obtain the present value of the option. Such a mechanism would seek to find the value of $v(x_{m-1}, \sigma_{m-1}, t_{m-1})$ from $v(x_{m}, \sigma_{m}, t_{m})$ for each $m = M - 1, M-2, ..., 2$. In this approach we would recompute the option value at each early-exercise date starting at \eqref{eq:early-exercise-call-val} or \eqref{eq:early-exercise-put-val} and proceeding as appropriate. However, \cite{fang2011fourier} proposed an improved approach which does not require the full valuation of the option at each early-exercise time, only the computation of its cosine series coefficients at the quadrature nodes. Once we have transported these coefficients back to the present time we can proceed to value the option as expected. This achieves a performance bonus in contrast to the trivial approach suggested above. The remainder of the section discusses the mechanism of this improved approach.

We assume that the values of $J$ and $N$ discussed above are defined. Using a suitable numerical integration method we can approximate the integral in \eqref{eq:V_nj} and we denote such an approximation by $\hat{V}_{k,p}(t_{m+1})$. Similarly we will denote by $\hat{C}_{k,p}(l, u, t_{m})$ the approximation to the $k^\text{th}$ cosine coefficient of the continuation function $c_3(x_{m}, \sigma_{m}, t_m )$. Finally $G_k(l,u)$ will denote the $k^\text{th}$ cosine coefficient of the payoff function $g(x_M, t_M)$. For convenience we collect each of these coefficients into the $N$ by $J$ matrices $\hat{V}(t_m)$, $\hat{C}(l, u, t_m)$ and $\hat{G}(l, u)$.

Consider an early exercise time $t_m$ ($m < M$). We know that the option value may be equal to its continuation function if early-exercise is not optimal. If this is the case then the cosine-coefficients of these functions are also equal. In addition at expiry we are certain that the cosine coefficients of the options value is equal to those of the payoff function. With this is mind, Fang and Oosterlee demonstrate the following decomposition of the cosine coefficient

\begin{equation}\label{eq:V=C+G}
\hat{V}(t_m) = \begin{cases}
\hat{C}(x^{*}(\varsigma_{p}, t_{m}), b, t_m) + G(a, x^{*}(\varsigma_{p}, t_m)) & \text{ for a put, } \\ 
\hat{C}(a, x^{*}(\varsigma_{p}, t_{m}), t_m) + G(x^{*}(\varsigma_p, t_m), b) & \text{ for a call. }  
\end{cases}
\end{equation}

For the exact details of this derivation and expressions for $\hat{C}_{k,p}(l, u, t_{m})$ and $G_k(l,u)$ the reader is encouraged to consult section 3.4 in \cite{FangOosterleeOriginalPaper}. 

For each quadrature node $\varsigma_j$ we have the $N$ by $P$ joint-probability density matrix $\tilde{\varphi}(\varsigma_j)$ consisting of elements $\tilde{\varphi}(\frac{n\pi}{b-a}, \varsigma_j, \varsigma_p)$. It is a joint-probability density matrix because $\tilde{\varphi}(\frac{n\pi}{b-a}, \varsigma_j, \varsigma_p) = p_{\ln(\nu)}(\varsigma_j | \varsigma_p) \varphi(\frac{n\pi}{b-a}; 0, e^{\varsigma_j}, e^{\varsigma_p})$. The density function $p_{\ln(\nu)}$ is available analytically in \cite{FangOosterleeOriginalPaper} and $\varphi$ in given in \eqref{eq:varphi}. Note that $\tilde{\varphi}(\varsigma_j)$ is time-invariant and so need only be computed once.

Consider now the cosine coefficients of the inner integrand in \eqref{eq:continuation-fn-sub}. We can evaluate the inner integral of these coefficients at each quadrature node $\varsigma_j$ with the following expression

\begin{equation}\label{eq:beta-j}
\hat{\beta}_j(t_{m-1}) := \left[ \hat{V}(t_m) \cdot \tilde{\varphi}(\varsigma_j) \right] \mathbf{w}
\end{equation}
where $\cdot$ denotes element-wise matrix multiplication and $\mathbf{w}$ is the column vector of the J quadrature weights. It is important to note that this relationship is key to jumping from one exercise point $t_m$ to the previous one $t_{m-1}$. We collect this evaluation at all quadratures nodes to form a matrix $\hat{B}(t_{m-1})$ given by

\begin{equation}\label{eq:B-hat}
\hat{B}(t_{m-1}) := [\hat{\beta}_{0}(t_{m-1}),\hat{\beta}_{1}(t_{m-1}),\dots,
\hat{\beta}_{J-1}(t_{m-1})],
\end{equation}

This allows us to evaluate the cosine coefficients of \eqref{eq:continuation_COS} as follows

\begin{equation}\label{eq:C-hat-expanded}
\hat{C}(l,u,t_{m-1}) := e^{-r\Delta t}\text{Re}\{ \mathcal{M}(l,u)\hat{B}^{\prime}(t_{m-1}) \}
\end{equation}
where $\mathcal{M}(l,u)$ is given in \cite{Fang2009}

The final point of note is that we know $\hat{V}(t_M)$ by virtue of the fact that we know the option is equal to its payoff function. 

For each $p = 0, 1, ... J$ we can work back to $\hat{V}(t_{M-1})$ as follows
\begin{enumerate}
\item Determine the critical stock price $x^{*}(\varsigma_{p}, t_{{M-1}})$ using a root finding method to seek $c_3(x_{M-1}, \varsigma_j, t_{M-1}) - g(x_{M-1}) = 0$.

\item Calculate $\hat{\beta}_j(t_{M-1})$ using \eqref{eq:beta-j} for each $j = 0, 1, ..., J-1$ and collect into $\hat{B}(t_{M-1})$ through \eqref{eq:B-hat}

\item Use \eqref{eq:C-hat-expanded} to calculate $\hat{C}(x^{*}(\varsigma_{p}, t_{M-1}), b, t_{M-1})$ for a put or $\hat{C}(a, x^{*}(\varsigma_{p}, t_{M-1}), t_{M-1})$ for a call.

\item Find $G(a, x^{*}(\varsigma_{p}, t_{M-1}))$ for a put and $G(x^{*}(\varsigma_p, t_{M-1}), b)$ for a call

\item Obtain $\hat{V}(t_{M-1})$ using \eqref{eq:V=C+G}
\end{enumerate}

From $\hat{V}(t_{M-1})$ we repeat the same procedure again and again until we obtain $\hat{V}(t_{1})$. With this we can obtain $\hat{v}(x, \varsigma_j, t_0)$ via \eqref{eq:continuation_COS} for each quadrature node $\varsigma_j$. Finally we use spline interpolation to obtain $\hat{v}(x, \sigma_0, t_0)$ as required.