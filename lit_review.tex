The first approach which used fast Fourier transforms (FFT) in option pricing was developed by \cite{carr1999option}. They confront the problem that for many models more sophisticated than the jump-diffusion model, the associated risk-neutral probability density function is unknown. This is the case for \cite{Heston1993} 's stochastic volatility model, \cite{BakshiChen} 's stochastic interest rate model and \cite{madan1998variance} 's Variance Gamma (VG) process, among many others. 
 
Fortunately in most of these cases the characteristic function associated with this unknown probability density is actually available in closed-form. The key insight of Carr and Madan was to recognise that the characteristic function essentially represents a Fourier transform of the unknown density function. Therefore after transforming the remainder of the system to the Fourier domain the system can be solved. All that is left is to apply the inverse Fourier transform to extract the option price.

The COS method, originally developed by \cite{FangOosterleeOriginalPaper}, gives a similar approach to option pricing. This method also employs a Fourier transform to exploit the relative availability of characteristic functions. 

A key point of difference is found in a log-transformation of the system in the lead up to the Fourier transform. While Carr and Madan take a log-transformation with respect to the strike price, Fang and Oosterlee take a log transformation with respect to the stock price. As indicated by Fang and Oosterlee, this has important implications in the inverse Fourier step and on the overall performance of the method. Therefore Fang and Oosterlee aim to improve upon the performance of the original FFT-based method so as to price quickly for a range of strike prices.

The starting point for our COS method analysis is to relate the value of an option to the expected future payoff discounted back to the present time. For instance consider European options in a risk-neutral world. We have the following risk-neutral valuation 

\begin{center}
$
\upsilon(x,t_{0}) = e^{-r\Delta t}\mathds{E}^{\mathds{Q}}[\upsilon(y,T)|x]= e^{-r\Delta t}\int_{\mathbb{R}} \upsilon(y,T) f^{\mathds{Q}}(y|x)dy
$
\end{center}
where,

$t_0$ is the present time. 

$x$ is the log-stock price at the present time and is given by $x = ln(S)$. 

$\upsilon(x,t_{0})$ is the option value at the present time. 

$\Delta t$ is the time difference between the maturity T and the present time $t_0$. 

$\mathds{E}^{\mathds{Q}}[\cdot]$  is the expectation operator under risk-neutral assumptions. 

$f^{\mathds{Q}}(y|x)$ is the probability density function of $y$ given $x$. 

$r$ is the risk-free interest rate.

In order to transform a function $f(x)$ into and from the Fourier domain we use the forward or inverse Fourier integrals (Fourier pair) 

\begin{center}
$
\phi(\omega) = \int_{\mathbb{R}}e^{ix\omega}f(x)dx,
$
$
f(x) = \frac{1}{2\pi}\int_{\mathbb{R}}e^{-i\omega x}\phi(\omega)d\omega
$
\end{center}

Once in the Fourier domain we approximate the Fourier-cosine expansion and then solve the inverse Fourier integral using numerical techniques. So
the idea is to reconstruct the whole integral from its Fourier-cosine series expansion.

\clearpage
The COS method can be applied to a variety of options under the condition that the Fourier transform exists. The condition for the existence of a Fourier transform is that the integrands (inside Fourier pair) have to decay to zero at both negative and positive limits. Therefore it is possible to truncate the integration range in an appropriate way without losing accuracy. Hence we can use numerical techniques to evaluate the integral.

Using the COS method Fang and Oosterlee were able to compute the option value for European options with a variety of strike prices $K$. In a later paper, \cite{Fang2009} they show that the COS method can also price early-exercise options under exponential L\' evy processes. Here they consider a Bermudan option with an equivalent payoff function at expiry as a comparable European option, but with an early-exercise payoff given by

$$
v(x,t_{m}) = max(g(x,t_{m}),c(x,t_{m})),
$$
where $g(x,t_{m})$ is the payoff when the holder chooses to exercise at time $t_m$ and $c(x,t_{m})$ is the value of the option if the holder retains it.  
They found that the COS method performs well for pricing Bermudan option under L\' evy processes. Importantly they showed that the COS method for European options can be extended to early-exercise options.

\cite{fastHilbert} price the Bermudan option under time-changed L\' evy processes by applying the fast Hilbert transform method and they contrast it to fast-Fourier-type methods such as the COS method. To benchmark the two methods they considered a variety of Bermudan put options. They considered both Heston's model and the inverse Gauss process time-changed by the CIR process. They compared the values obtained from the fast Hilbert transform algorithm to those from the COS method. In addition they contrasted the run-time of each algorithm in each case. They found that the fast Hilbert method provides highly accurate and more efficient results when compared to the COS method.
