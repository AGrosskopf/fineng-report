\section{Results}\label{sec:result}
In this section we will consider the performance of our method. In order to verify whether it is working correctly we will compare the values given by our algorithm to the available reference values listed in \cite{fang2011fourier}. We will also assess effect what if any, the parameters $J$ (quadrature nodes) and $N$ (Fourier series truncation) have on the accuracy and stability of implementation. Finally we will examine the performance of our algorithm in relation to these parameter changes.

\subsection{Benchmarking}\label{sub-sec:benchmarks}
In their fourth test \cite{fang2011fourier} provide some reference values with which we intend to verify the results given by our program. We set the number of exercise dates to be $M = 10$, the number of Fourier terms considered was $N = 256$, the number of nodes in the equidistant quadrature was $J = 16$. Table \ref{tbl:benchmark-1} gives the results from our programme, the reference values and the associated absolute and relative errors for these tests across the range of values of $S_0$ considered by Fang and Oosterlee.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\textbf{$S\_\{0\}$} & \textbf{C++} & \textbf{Ref. Value} & \textbf{Absolute Error} & \textbf{Relative Error} & \textbf{Time (s)} \\ \hline
8                 & 2.4788       & 2                   & 0.4788                  & 0.2394                  & 0.71              \\ \hline
9                 & 1.3781       & 1.1076              & 0.2705                  & 0.2442                  & 0.712             \\ \hline
10                & 0.6141       & 0.5200              & 0.0941                  & 0.1809                  & 0.712             \\ \hline
11                & 0.2171       & 0.2137              & 0.0035                  & 0.0162                  & 0.73              \\ \hline
12                & 0.0620       & 0.0820              & 0.0201                  & -0.2445                 & 0.725             \\ \hline
\end{tabular}
\caption{The benchmark results for Test No. 4 with $M = 10$}
\label{tbl:benchmark-1} 
\end{table}

Additionally we rerun the test for $M = 20$ exercise dates. All other parameters remain unchanged. Table \ref{tbl:benchmark-2} reports the results for these tests.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\textbf{$S\_\{0\}$} & \textbf{C++} & \textbf{Ref. Value} & \textbf{Absolute Error} & \textbf{Relative Error} & \textbf{Time (s)} \\ \hline
8                 & 2.4727       & 2                   & 0.47272                 & 0.23636                 & 0.725             \\ \hline
9                 & 1.2796       & 1.10762             & 0.171939                & 0.1552327               & 0.737             \\ \hline
10                & 0.4360       & 0.52003             & 0.083981                & -0.1614926              & 0.729             \\ \hline
11                & 0.08815      & 0.21368             & 0.125525                & -0.5874540              & 0.721             \\ \hline
12                & 0.01058      & 0.08204             & 0.071466                & -0.8710692              & 0.727             \\ \hline
\end{tabular}
\caption{The benchmark results for Test No. 4 with $M = 20$}
\label{tbl:benchmark-2}
\end{table}

We can make two general statements about the data presented above.
\begin{enumerate}
	\item $M = 10$ gives more accurate results than $M = 20$.
	\item As $S$ increases, the accuracy of the results also increases.
\end{enumerate}

Both of the above statements also hold true when considering the results presented by \cite{fang2011fourier}.

We can illustrate the option price given by our programme and the relative error as follows. In figure \ref{fig:opt-vals} we plot the option values returned by our program across the range of benchmarking tests run above. Figure \ref{fig:opt-vals-relerr} takes the relative error of these benchmarks. 

\begin{figure}[h!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Figure/option_value.png}
  \caption{The option values given by C++ implementation}
  \label{fig:opt-vals}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{Figure/Surface_error_RE.png}
  \caption{The relative error in the C++ values}
  \label{fig:opt-vals-relerr}
\end{subfigure}
\end{figure}

Now we consider the results for the Test No 5. In \cite{fang2011fourier} the results are novel reference values in the sense that the authors could not find a reference value in existing literature at the time of publication and so provided the results from their method. Here we treat those values as reference values. We fix $N = 256$ and $J = 16$ as before. Table {tbl:benchmark-3} reports the results from these tests for the same range of $S_0$ as considered by Fang and Oosterlee.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|}
\hline
\textbf{$S_{0}$} & \textbf{C++} & \textbf{Time (s)} \\ \hline
90                & 6.4442       & 0.68              \\ \hline
100               & 2.88747      & 0.699             \\ \hline
110               & 0.923728     & 0.712             \\ \hline
\end{tabular}
\caption{The benchmark results for Test No. 5 with $M = 10$}
\label{tbl:benchmark-3}
\end{table}

Unfortunately we were unable to reproduce the reference values for both tests when $M \geq 40$, or for $M = 20$ in Test No 5. The results reported by our program were affected by the presence of a NaN values. Further analysis shows that the root cause of this is our implementation of the modified Bessel function. There NaN propagates up via \verb|LogVarConditionalChF| to $\tilde{\phi}(\zeta_j)$ and from there affects all the values in the algorithm. We believe that if a more robust implementation of the modified Bessel function were substituted into the programme, we could obtain sensible values for $M = 40$ and $M = 80$.

\subsection{Effect of $N$ and $J$}
We now turn our attention to the problem of measuring the effect of the parameters inputted into the the algorithm have on the accuracy and speed of our implementation. In particular we consider perturbations in the number of nodes in the quadrature given by $J$ and the number of terms included in the truncation of the Fourier series $N$. For the first test we considered the range $N \in \{16, 32, 64, 128, 256\}$ whilst $J = 16$ and $M = 10$. The results for these tests are displayed in table \ref{tbl:varN-tests}.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|l|l|}
\hline
\textbf{$S_{0}$}  & \textbf{C++} & \textbf{Ref. Val} & \textbf{\begin{tabular}[c]{@{}c@{}}Absolute\\   Error\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Relative\\ Error\end{tabular}} & \textbf{Time (s)} & N   & J                   \\ \hline
\multirow{5}{*}{8} & 2.41199      & 2                 & 0.41199                                                            & 0.205995                                                          & 0.045             & 16  & \multirow{5}{*}{16} \\ \cline{2-7}
                   & 2.4741       & 2                 & 0.4741                                                             & 0.23705                                                           & 0.086             & 32  &                     \\ \cline{2-7}
                   & 2.4788       & 2                 & 0.4788                                                             & 0.2394                                                            & 0.186             & 64  &                     \\ \cline{2-7}
                   & 2.4788       & 2                 & 0.4788                                                             & 0.2394                                                            & 0.357             & 128 &                     \\ \cline{2-7}
                   & 2.4788       & 2                 & 0.4788                                                             & 0.2394                                                            & 0.706             & 256 &                     \\ \hline
\end{tabular}
\caption{The effects of varying the parameter $N$}
\label{tbl:varN-tests}
\end{table}

We see that the option value converges but with a bias of $|2 - 2.4788| = 0.4788$. We also note that the convergence appears to happen around $N\in(32, 64)$. 

Next we examine the effects of the number of nodes in the quadrature $J$. We consider the range of values $J in \{4, 8, 12, 16, 32\}$. We hold the parameters $N$ and $M$ constant with $N = 256$ and $M = 10$. The results from these tests are listed in table \ref{tbl:varJ-tests}.
